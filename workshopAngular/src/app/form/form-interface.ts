export interface FormInterface {
    fname: string;
    lname: string;
    age: number;
    year: number;
    university: string;
    linkfb: string;
}
