import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormInterface } from '../form-interface';

@Injectable({
  providedIn: 'root'
})
export class FormInputService {

  dataForm = new BehaviorSubject<FormInterface>({} as any);
  currentDataForm = this.dataForm.asObservable();

  constructor(

  ) { }

  setDataForm(meassage: any): void{
    this.dataForm.next(meassage);
  }
  
}
