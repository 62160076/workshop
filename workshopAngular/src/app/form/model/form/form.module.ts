import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormRoutingModule } from './form-routing.module';
import { FormInputComponent } from '../../form-input/form-input.component';
import { FormShowComponent } from '../../form-show/form-show.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormInputService } from '../../service/form-input.service';
import { ModalComponent } from '../../modal/modal.component';


@NgModule({
  declarations: [
    FormInputComponent,
    FormShowComponent,
    ModalComponent
  ],
  imports: [
    CommonModule,
    FormRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [
    FormInputService
  ]
})
export class FormModule { }
