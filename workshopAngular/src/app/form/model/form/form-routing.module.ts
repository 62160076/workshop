import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormInputComponent } from '../../form-input/form-input.component';
import { FormShowComponent } from '../../form-show/form-show.component';

const routes: Routes = [
  {
    path: 'formInput',
    component: FormInputComponent,
  },
  {
    path: 'formShow',
    component: FormShowComponent
  },
  {
    path: '', 
    redirectTo: 'formInput', 
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormRoutingModule { }
