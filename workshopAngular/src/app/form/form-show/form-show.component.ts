import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import { FormInputComponent } from '../form-input/form-input.component';
import { FormInterface } from '../form-interface';
import { FormInputService } from '../service/form-input.service';

@Component({
  selector: 'app-form-show',
  templateUrl: './form-show.component.html',
  styleUrls: ['./form-show.component.css']
})
export class FormShowComponent implements OnInit {

  fname: string = ' ';
  lname: string = ' ';
  age: any;
  year: any;
  university: string = ' ';
  linkfb: any;
  data: any;

  constructor(
    private route: Router,
    private formInputService: FormInputService
  ) { }

  ngOnInit(): void {
    this.formInputService.currentDataForm.subscribe(res => {
      this.fname = res.fname
      this.lname = res.lname;
      this.age = res.age;
      this.year = res.year;
      this.university = res.university;
      this.linkfb = res.linkfb;

      this.formInputService.setDataForm(res);
    });
  }
  
  back(){
    this.route.navigate(['formInput']);
  }

}
