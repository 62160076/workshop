import { Component, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalDismissReasons, NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { FormInterface } from '../form-interface';
import { FormShowComponent } from '../form-show/form-show.component';
import { ModalComponent } from '../modal/modal.component';
import { FormInputService } from '../service/form-input.service';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.css'],
  encapsulation: ViewEncapsulation.None,
  styles: [`
    .modal-backdrop {
      z-index: 1040 !important;
    }
  `]
})
export class FormInputComponent implements OnInit {

  form = new FormGroup({
    fname: new FormControl('',Validators.required),
    lname: new FormControl('',Validators.required),
    age: new FormControl('',Validators.required),
    year: new FormControl('',Validators.required),
    university: new FormControl('',Validators.required),
    linkfb: new FormControl(''),
  })

  options: NgbModalOptions = {
    animation: true,             
    centered: true,
  };
  lname!: string;
  fname!: string;
  age!: number;
  year!: number;
  university!: string;
  linkfb!: string;


  constructor(
    private route: Router,
    private formInputService: FormInputService,
    private modalService: NgbModal
  ) { 

  }

  ngOnInit(): void {
    if(this.formInputService.currentDataForm != null){
      this.formInputService.currentDataForm.subscribe(res => {
        this.fname = res.fname
        this.lname = res.lname;
        this.age = res.age;
        this.year = res.year;
        this.university = res.university;
        this.linkfb = res.linkfb;
      });
    }
  }

  submit(){
    if(this.form.invalid){
      const modalRef = this.modalService.open(ModalComponent,this.options);
      modalRef.componentInstance.subtitle = 'กรุณากรอกข้อมูลให้ครบถ้วน';
    }else if(this.form.valid){
      this.formInputService.setDataForm(this.form.value);
      this.route.navigate(['formShow']);
    }
  }

}

