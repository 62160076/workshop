import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormInputComponent } from './form/form-input/form-input.component';

const routes: Routes = [
  {
    path: '',
    component: FormInputComponent,
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
